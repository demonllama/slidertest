package com.tinman.cmslidertest;


import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends Activity {
	
	//============
	//Declare all values which will be obtained from a savedInstanceState
	//============
	private static final String BILL_TEXT = "BILL_TEXT";
	private static final String TIP_TEXT = "TIP_TEXT";
	private static final String TOTAL_TEXT = "TOTAL_TEXT";
	
	
	//============
	//Declare all values which will be obtained from the UI
	//============
	EditText billNetText;
	EditText billTipText;
	EditText billTotalText;
	SeekBar seekBar;
	
	
	//============
	//Declare all values which will be used within this class
	//============
	double billNetValue;
	double billTipValue;
	double billTotalValue;
	
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//============
		//Check if the application is restoring from a prior state
		//============
		if(savedInstanceState == null){
			
			billNetValue = 0;
			billTipValue = 0;
			billTotalValue = 0;
			
			
		}else{
			
			billNetValue = savedInstanceState.getDouble(BILL_TEXT);
			billTipValue = savedInstanceState.getDouble(TIP_TEXT);
			billTotalValue = savedInstanceState.getDouble(TOTAL_TEXT);
			
		}
		
		//============
		//Initialise the EditTexts. Although will only be using billNetText to listen to
		//============
		billNetText = (EditText) findViewById(R.id.billNetText);
		billTipText = (EditText) findViewById(R.id.billTipText);
		billTotalText = (EditText) findViewById(R.id.billTotalText);
		
		//Add billNetText listener
		billNetText.addTextChangedListener(billBeforeTipChangeListener);
		
		//============
		//Initialise seek bar
		//============
		seekBar = (SeekBar) findViewById(R.id.seekBar);
		
		//Add SeekBar Listener
		seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
		
		
		
		
	}
	
	protected void updateFinalBill() {
		
		billTotalValue = billNetValue + (billNetValue * billTipValue);
		billTotalText.setText(String.format("%.2f", billTotalValue));
		
	}
	
	
	//============
	//Listener method for billTipValue
	//============
	
	private TextWatcher billBeforeTipChangeListener = new TextWatcher(){

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			
			try{
				
			
			billNetValue = Double.parseDouble(arg0.toString());
			} catch (NumberFormatException e){
				billNetValue = 0.0;
			}
			
			updateFinalBill();
			
		}
		
	};
	
	//============
	//Listener method for seek bar
	//============
	private OnSeekBarChangeListener onSeekBarChangeListener = new OnSeekBarChangeListener(){

		@Override
		public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
			
			billTipValue = seekBar.getProgress() * 0.01;
			billTipText.setText(Double.toString(billTipValue));
			
			updateFinalBill();
			
		}

		@Override
		public void onStartTrackingTouch(SeekBar arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar arg0) {
			// TODO Auto-generated method stub
			
		}
		
	};
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}




}
